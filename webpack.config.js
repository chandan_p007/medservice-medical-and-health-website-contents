const currentTask = process.env.npm_lifecycle_event;//givrs the name of the current tasl (dev/build)
const path = require('path');

const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const fse = require('fs-extra');

const postCSSplugins = [
    require('postcss-import'),
    require('postcss-mixins'),
    require('postcss-simple-vars'),
    require('postcss-nested'),
    require('autoprefixer')
];

class TaskAfterCompilation{
    apply(compiler){
        compiler.hooks.done.tap('Copy Images...',function(){
            fse.copySync('./app/assets/images','./dist/assets/images');
        });
    }
}


let cssConfig = {
    test: /\.css$/i,
    use:[
    'css-loader?url=false',
        
    {
        loader: 'postcss-loader',
        options: {
            plugins: postCSSplugins
        }
    }
],
};

let config = {
    entry: './app/assets/Scripts/app.js',
    plugins: [
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: "./app/index.html",
            // minify: false
        })
    ],
    module: {
        rules: [
               cssConfig
        ]
    }
};

if(currentTask == "dev"){
    config.output = {
        filename: "app.bundled.js",
        path: path.resolve(__dirname,"app")
    };
    config.devServer = {
        before: function (app,server) {
          server._watch('./app/**/*.html');
        },
        contentBase: path.join(__dirname,"app"),
        hot: true,
        port: 3000,
        host: '0.0.0.0'
    };
    config.mode = "development";

    cssConfig.use.unshift('style-loader');

}

if(currentTask == "build"){
    config.output = {
        filename: "[name].[chunkhash].js",
        chunkFilename: "[name].[chunkhash].js",
        path: path.resolve(__dirname,"dist"),
    };
    config.mode = "production";
    config.optimization = {
        splitChunks: {
            chunks: "all"
        }
    };
    config.plugins.push(
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: 'styles.[chunkhash].css'
        }),
        new TaskAfterCompilation(),
);

    cssConfig.use.unshift(MiniCssExtractPlugin.loader);
    postCSSplugins.push(require('cssnano'));
    
}



module.exports = config;
