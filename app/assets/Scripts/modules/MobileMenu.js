//1.select the element
//2.add listener to item
// 3.write a to perform event action

// document.querySelector("selector").addEventListener("click",function(){

// });

class MobileMenu{
    constructor(){
        //select the element
        this.menuIcon = document.querySelector(".mobile-header-icon");
        this.mobileHeader = document.querySelector(".mobile-menu");
        this.events();
    }

    events(){
        this.menuIcon.addEventListener("click",()=>this.toggleMenu());
    }
    toggleMenu(){
        this.mobileHeader.classList.toggle("mobile-menu-active");
        this.menuIcon.classList.toggle("mobile-header-icon-close");
    }
}

export default MobileMenu;