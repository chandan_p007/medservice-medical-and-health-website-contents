import $ from 'jquery';
class Modal{
    constructor(){
        this.openModalButtons = $(".open-modal");
        this.modal = $(".modal");
        this.modalCloseButton = $(".modal-close");
        this.events();

    }

    events(){
        this.openModalButtons.click(this.openModal.bind(this));
        //arrow operator automatically binds this to the callback with main object

        this.modalCloseButton.click(this.closeModal.bind(this));
        $(document).keyup(this.KeyPressHandler.bind(this));
    }

    openModal(){
        console.log("modal opened");
        this.modal.addClass("modal-is-visible");
        return false;
    }
    closeModal(){
        this.modal.removeClass("modal-is-visible");
    }

    KeyPressHandler(e){
        if(e.keyCode == 27){
            this.closeModal();
        }
    }
}

export default Modal;